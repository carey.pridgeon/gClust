
/**
* This class represents a particle in Moody.
* Particles are responsible for their own movement, so contain internal integration methods.
*/
#include <iostream>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include <cmath>
#include "Advisor.h"
#include "Numerics.h"
#include "ParticleStruct.h"
#include "ParamClass.h"
#include "glm/glm.hpp"

class Particle {
private:
    private:
  int id;
    /**
    * the output file stream;
    */
    std::ofstream outFileStream;
    /**
    * the filename to be used
    */
    std::string filename;


    float radius;
    float mass;

	/**
	* The position of this particle 
	*/
	float x;
	float y;
	float z;
	/**
	* The velocity of this particle
	*/
	float xd;
	float yd;
	float zd;



	
	Numerics maths;



	/**
	* the red portion of this particles colour
	*/
	int red;

	/**
	* the green portion of this particles colour
	*/
	int green;

	/**
	* the blue portion of this particles colour
	*/
	int blue;

	/**
	* the std::string name of this particle
	*/
	std::string name;

	/**
	*  What types of particle this particle is allowed to interact with.
	*/
	int interactionPermission;

	/**
	* what type this particle is - (collapsor/normal particle/placemark/spacecraft)
	*/
	int identity;

	/**
	*  what size this particle will have when displayed in a visualisation
	*/
	int visualRepresentation;

	/**
	* Mathematical functions and operations
	*/
	//	Numerics maths;

	

    
public:
 
    	Particle(){};
 	~Particle(){};

	//cluster is extended along y axis	
	void makeCluster(ParamClass p) {
	double orbit_tmp;
	double pc;
	double pc2;
	double pc3;
	double tmp;
	double chance;
	double curlyThing,theta;
	ParticleStruct content;
        int tot = 1;
	double distanceThis;
	double distance2;

	while  (tot<pCount) {


		// place this particle at origin
		content.x = p.xin;
		content.y = p.yin;
		content.z = p.zin;

		//set some random mass
		content.mass =glm::linearRand(p.minMass,p.maxMass); //double
	        //std::cout << "> Particle mass "<< content.mass <<std::endl;

		content.radius = maths.randomRangeInt2;
		content.id = tot;
		content.visualRepresentation = glm::linearRand( p.minRadius, p.maxRadius); //int
		content.name = "";
		content.identity = p.identity //106;
		content.interactionPermission = p.interactionPermission. 1;

		//straight line distance from mindistance to maxdistance
		orbit_tmp = glm::linearRand(0,p.distance);


	    // add the curve to the z axis via magic

		       
		  content.red = p.red; 
		  content.green = p.green; 
		  content.blue = p.blue; 
		  content.xd = p.xd; 
		  content.yd = pd.yd; 
		  content.zd = p.zd;
	      
		  //transform
		  // move to correct place
		 content.x += p.xin;
		content.y += p.yin;
		content.z += p.zin;




		  
                  this->fillFromExistingParticleStruct(content);
                  this->exportAsXMLToParticlesFile();
	          tot++;
		if (tot == pCount) {//done
	  break;
	}
    }
	
}

    

	

	void makeGlob(double minDistance, double maxDistance, double nMass,int mult, int pCount, int r,int g, int b) {
	double orbit_tmp;
	double tmp= 0;
	double curlyThing,theta;
	    ParticleStruct content;
	int i;

    for (i=0;i<pCount;i++) {
	    std::cout << "> Paricle  "<< i <<std::endl;

		// place this particle at origin
		content.x = 0;
		content.y = 0;
		content.z = 0;

		//set some random mass
		content.mass =maths.randomRange(nMass,nMass*mult); //double
	        std::cout << "> Particle mass "<< content.mass <<std::endl;

		content.radius = 2;
		content.id = i;
		content.visualRepresentation = maths.randomRangeInt( 1, 30); //int
		content.name = "n";
		content.identity = 106;
		content.interactionPermission = 1;
		// The orbital height needs to have it's distance from origin added
		orbit_tmp = maths.randomRange(minDistance,maxDistance);
		
		std::cout << "> Particle distance "<< orbit_tmp <<std::endl;

		// Pi is presented as an int here, but the result is divided, returning the result to its proper form
		curlyThing = ((double)(rand() % (2*3141592)))/100;
		theta = ((float)(rand() % 3141592))/100;
		// do all the rotation shit
		content.x += orbit_tmp*(cos(curlyThing)*sin(theta));
		content.y += orbit_tmp*(sin(curlyThing)*sin(theta));
		content.z += orbit_tmp*(cos(theta));
		content.red = r;
		content.green = g;
		content.blue = b;
		content.xd = 0;
		content.yd = 0;
		content.zd = 0;

        this->fillFromExistingParticleStruct(content);
        this->exportAsXMLToParticlesFile();
    }
    //add the distant black hole to screw wth this cluster
    content.mass =nMass*40000; //double
    std::cout << "> black hole mass "<< content.mass <<std::endl;
    content.radius = 7;
    content.id = i;
    content.visualRepresentation = maths.randomRangeInt( 1, 30); //int
    content.name = "Black Hole";
    content.identity = 106;
    content.interactionPermission = 1;    
    content.x = 0;
    content.y = maxDistance *2;
    content.z = 0;

    std::cout << "> black hole ypos "<< content.y <<std::endl;

    content.xd = 0;
    content.yd = -(2.1*1000000)*1000;
        std::cout << "> black hole yd "<< content.yd <<std::endl;

    content.zd = 0;
    this->fillFromExistingParticleStruct(content);
    this->exportAsXMLToParticlesFile();
}




  
    void startXMLFile() {
        std::stringstream out;
        out <<"<root>" <<std::endl;
        std::string str = out.str();
        this->outFileStream.open(this->filename.c_str(),std::ios::app);
        this->outFileStream << str;
        this->outFileStream.close();
    }

    void finishXMLFile() {
        std::stringstream out;
        out <<"</root>" <<std::endl;
        std::string str = out.str();
        this->outFileStream.open(this->filename.c_str(),std::ios::app);
        this->outFileStream << str;
        this->outFileStream.close();
    }  
    
    void setFileName() {
        this->filename = "particles.xml";
    }
 
    void exportAsXMLToParticlesFile() {


        std::stringstream out;
        out <<"  <particle>" <<std::endl;
        out <<"   <interactionPermission>";
        if (this->interactionPermission == Advisor::interactALL) {
            out <<"interactALL";
        }
        if (this->interactionPermission == Advisor::interactNONE) {
            out <<"interactNONE";
        }
        if (this->interactionPermission == Advisor::interactEnvironmentOnly ) {
            out <<"interactEnvironmentOnly";
        }
        if (this->interactionPermission == Advisor::interactDifferentOnly) {
            out <<"interactDifferentOnly";
        }
        out <<"</interactionPermission>" <<std::endl;
        out <<"   <identity>";
        if (this->identity == Advisor::collapsor) {
            out <<"collapsor";
        }
        if (this->identity == Advisor::collapsorFixed) {
            out <<"collapsorFixed";
        }
        if (this->identity == Advisor::nonInteractive) {
            out <<"nonInteractive";
        }
        if (this->identity == Advisor::ordinary) {
            out <<"ordinary";
        }
        if (this->identity == Advisor::planetesimal) {
            out <<"planetesimal";
        }
        if (this->identity == Advisor::chromosome) {
            out <<"chromosome";
        }
        out <<"</identity>" <<std::endl;
 	out << " <id>"<< this->id<< "</id>" <<std::endl;
        out <<"   <name>"<< this->name<<"</name>" <<std::endl;
        out <<"   <visualSize>"<<this->visualRepresentation <<"</visualSize>" <<std::endl;
        out <<"   <rgb>" <<std::endl;
        out <<"    <red>"<<this->red <<"</red>" <<std::endl;
        out <<"    <green>"<<this->green <<"</green>" <<std::endl;
        out <<"    <blue>"<<this->blue <<"</blue>" <<std::endl;
        out <<"   </rgb>" <<std::endl;
        out <<"   <mass>"<<std::scientific<< this->mass<<"</mass>" <<std::endl;
        out <<"   <radius>"<<std::scientific<< this->radius <<"</radius>" <<std::endl;
        out <<"   <vector>" <<std::endl;
        out <<"    <X>"<<std::scientific<<this->x <<"</X>" <<std::endl;
        out <<"    <Y>"<<std::scientific<<this->y <<"</Y>" <<std::endl;
        out <<"    <Z>"<<std::scientific<<this->z <<"</Z>" <<std::endl;
        out <<"    <XD>"<<std::scientific<<this->xd <<"</XD>" <<std::endl;
        out <<"    <YD>"<<std::scientific<<this->yd<<"</YD>" <<std::endl;
        out <<"    <ZD>"<<std::scientific<<this->zd <<"</ZD>" <<std::endl;
        out <<"   </vector>"<<std::endl;
        out <<"  </particle>" <<std::endl;
        std::string str = out.str();
        this->outFileStream.open(this->filename.c_str(),std::ios::app);
        this->outFileStream << str;
        this->outFileStream.close();

    }




    /**
    *  fill all the private members of this particle using a particle structs content.
    * this option exists so a particles internal state can be extracted, changed and re-inserted.
    * It's not currently used, but may be useful for EA experiments
    */
    void fillFromExistingParticleStruct(ParticleStruct & p) {
        this->red = p.red;
        this->green = p.green;
        this->blue = p.blue;
	this->id = p.id;
        this->interactionPermission = p.interactionPermission;
        this->identity = p.identity;
        this->mass = p.mass;
        this->visualRepresentation = p.visualRepresentation;
        this->name = p.name;
        this->radius = p.radius;
        this->x = p.x;
        this->y = p.y;
	this->z = p.z;
        this->xd = p.xd;
        this->yd = p.yd;
	this->zd = p.zd;
    }




  };
