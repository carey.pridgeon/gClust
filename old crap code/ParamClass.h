

Class ParamClass {
 double xin;
 double yin;
 double zin;
 double xinTrans;
 double yinTrans;
 double zinTrans;
 double xRot;
 double yRot;
 double zRot;
 double xd;
 double yd;
 double zd;
 double Distance;
 double DistanceMult;
 double minMass;
 double maxMass;
 double minRadius;
 double maxRadius;
 int massMult;
 int minSize;
 int maxSize;
 int pCount;
 int red;
 int green;
 int blue;
 double cosAdd;
 double xTimesVal;
 double cosDivide;
 int identity;
 int interactionPermission;
}
