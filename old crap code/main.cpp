#include "Particle.h"
int main(int argc, char *argv[]) {
    Particle tmp;
    double distance = atof(argv[1]);
    double distPower =atof(argv[2]);
    double mass = atof (argv[3]);
    double massPower =atof(argv[4]);
    double mult= atoi (argv[5]);
    int pCount = atoi(argv[6]);
    Numerics maths;
    std::cout << "> pcount "<< pCount <<std::endl;

    distance =pow(distance,distPower) *1000;
    mass = pow(mass,massPower);
    maths.seedRand(time(NULL));
    tmp.setFileName();
    tmp.startXMLFile();
    //std::cout << ">gauss "<< maths.generateGaussian() <<std::endl;

    tmp.makeCluster(0,distance,mass,mult, pCount,26,92,178,0,0,0);
    tmp. finishXMLFile();
    std::cout << "> Sphere generated with "<< pCount<< " Particles" <<std::endl;
    return 0;
}
