#pragma once

#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include "mopStateUtils.h"
#include "compression.h"
bool writeStateToFile(std::ifstream *,std::string, mopState *);
bool consumeChar(std::ifstream *,char &);
void openMopfile(std::ifstream *,std::string);
void CloseMopfile(std::ifstream *);
void resetFile(std::ifstream *);
bool readStateFromFile(std::ifstream *, int);
void resetFile(std::ifstream *, mopState *);
inline double sqr( double);
//----------------------------
// state writing functionality
//----------------------------
bool writeStateToFile(std::string filename, mopState *in) {
  int i;
  std::ofstream outFileStream;
  outFileStream.open( filename.c_str(), std::ios::out | std::ios::app);
  for (i=0;i<in->size;i++) {
    //outFileStream << mopToString(in->content[i]);
  }
  outFileStream.close();
  return true;
}

//----------------------------
// state reading functionality
//----------------------------

// various mop file handling functions
bool consumeChar(std::ifstream *inFileStream,char &ch, int *fPos) {
  if (!inFileStream->eof()) {
    ch = inFileStream->get();
    *fPos= inFileStream->tellg();
    return true;
  }
  return false;
}

bool readStateFromFile(std::ifstream *inFileStream, int skip, mopState *in) {
  // are we at the end of the file?
  if (in->fPos == in->fSize) {
    // if so, reset
    resetFile(inFileStream,in);
  }
  // read in state, update fPos
  in->fPos= inFileStream->tellg();// where are we now?
  return true;
}

/**
 * open the mopfile for reading
 */
void openMopfile(std::ifstream *inFileStream,std::string filename, int *fPos) {
  inFileStream->close(); 
  *fPos = 0;
  try {
    inFileStream->open(filename.c_str(), std::ios_base::in);
    inFileStream->seekg(0, std::ios_base::beg);
  } catch (...) {
    std::cerr << "error: cannot open mop file" << std::endl;
    exit(0);
  }
}

/**
 * close the mopfile 
 */
void CloseMopfile(std::ifstream *inFileStream) {
  inFileStream->close(); 

}


/**
 * reset the mop file back to the start of the file
 */
void resetFile(std::ifstream *inFileStream, mopState *in) {
  inFileStream->seekg(0, std::ios_base::beg);
  in->fPos = 0;
}


inline double sqr( double x)  {
        return ((x)*(x));
}

bool  mopFileSize( const char* filePath, mopState *in){
    std::streampos fsize = 0;
    std::ifstream file( filePath, std::ios::binary );
    fsize = file.tellg();
    file.seekg( 0, std::ios::end );
    fsize = file.tellg() - fsize;
    file.close();
    in->fSize = static_cast < int > (fsize);
    return true;    
}
/**
 * mopState *in - the mopstate being processed
 * bool *firstState - control block that only gets run on the first state
 * int *firstMax - the furthest from origin particle distance in the first state
 * int * sc - the calculated scaler
 * int extent - the initial visibility limit (display context related, x or y axis)
 */

bool rescale(mopState *in, bool *firstState, int *firstMax, float * sc, int extent) {
  int j;

  if (firstState) { // establish the outer bound for scaling on the first iteration.
	  *firstState = false;
	  double xt;
	  double yt;
	  double zt;
	  double distancesqr;
	  int index = 0;
	  double distance;
	  *firstMax = 0;
	  for (j=0; j<in->size; j++) {
	    xt = sqr(in->content[j].x);
	    yt = sqr(in->content[j].y);
	    zt = sqr(in->content[j].z);
	    distancesqr=(xt+yt+zt);
	    distance = sqrt(distancesqr);
	    if (distance >*firstMax) {
	      index = j;
	      *firstMax = distance;
	    }
	  }
	}
	*sc = *firstMax/extent;


	for (j=0; j<in->size; j++) {
	  in->content[j].xs = in->content[j].x * (*sc);
	  in->content[j].ys = in->content[j].y * (*sc);
	  in->content[j].zs = in->content[j].z * (*sc);
	}
	

  
	return true;
}
	  /*	
        for (int x(0); x<in->size; x++) {
                m1 = sqrt(sqr(in->content[x].x));
                m1 = m1 /(maxDistanceFromOrigin+(0.5*maxDistanceFromOrigin));
                std::cout << "M1: " << m1 << std::endl;
                m2 = (sqrt(sqr(in->content[x].y))/(maxDistanceFromOrigin+(0.5*maxDistanceFromOrigin)));
                m3 = (sqrt(sqr(in->content[x].z))/(maxDistanceFromOrigin+(0.5*maxDistanceFromOrigin)));
                if (in->content[x].x>0) {
                        in->content[x].xs = m1*scaler;
                } else {
                        in->content[x].xs = m1*(scaler);
                }

                if (in->content[x].y>0) {
                        in->content[x].ys = m2*scaler;
                } else {
                        in->content[x].ys = m2*(scaler);
                        //std::cout << "Tmp Y: " << tmp.y << std::endl;
                }

                if (in->content[x].z>0) {
                        in->content[x].zs = m3*scaler;
                } else {
                        in->content[x].zs = m3*(scaler);
                }
        }*/
	

