This software is released under the Apache License 2.0
http://www.apache.org/licenses/LICENSE-2.0


There is no documentation yet. Not because I'm lazy, but because it's in such a state of flux right now there's literally no point.
I don't want to keep this codebase, that's certain.
